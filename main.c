#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

// the following fuctions return double in [0, 1)
double my_rand();
double classic_rand();

int main(int argc, char** argv) {
    // Convertion of initial data
    int a, b, x, particles_number, threads_number;
    float p;
    char* output_file_name = "stats.txt";
    if (!(argc == 7 || argc == 8)) {
        printf("Wrong number of arguments!\n");
        exit(-1);
    }
    a = atoi(argv[1]);
    b = atoi(argv[2]);
    x = atoi(argv[3]);
    if (x <= a || x >= b) {
        printf("Wrong numbers a, b or x! Must be a<x<b!\n");
        exit(-1);
    }
    particles_number = atoi(argv[4]);
    if (particles_number <= 0) {
        printf("The number of particles must be greater than zero!\n");
        exit(-1);
    }
    p = atof(argv[5]);
    if (p >= 1 || p <= 0) {
        printf("The probability p must be between 0 and 1!\n");
        exit(-1);
    }
    threads_number = atoi(argv[6]);
    if (threads_number < 1) {
        printf("The number of threads must be 1 or greater!\n");
        exit(-1);
    }
    if (argc == 8) {
        output_file_name = argv[7];
    }
    

    // Measurement 
    srand(time(NULL));
    omp_set_num_threads(threads_number);
    int successful_particles = 0;
    double all_particles_time = omp_get_wtime();
    long all_steps = 0;
    #pragma omp parallel for reduction(+:all_steps, successful_particles)
    for (int i = 0; i < particles_number; ++i) {
        int current_x = x;
        long steps = 0;
        while (current_x != a && current_x != b) {
            steps++;
            if (my_rand() <= p) {
                current_x++;
            } else {
                current_x--;
            }
        }
        all_steps += steps;
        if (current_x == b) {
            successful_particles++;
        }
    }
    all_particles_time = omp_get_wtime() - all_particles_time;
    
    
    // Output 
    FILE* file = fopen(output_file_name, "a");
    if (file == NULL) {
        printf("Problems with file!\n");
        exit(-1);
    }
    fprintf(file, "%f %f %fs %d %d %d %d %f %d\n", 
        (float) successful_particles / particles_number, 
        (double) all_steps / particles_number, all_particles_time,
        a, b, x, particles_number, p, threads_number);
    fclose(file);
    return 0;
}


double my_rand() {
    int random_int;
    static __thread unsigned int seed = 0;
    if (seed == 0) {
        // Изначально и в редких случаях заполняем seed случайным числом 
        seed = (unsigned int)rand();
    }
    random_int = rand_r(&seed);
    return (double)random_int / RAND_MAX;
}

double classic_rand() {
    return (double)rand() / RAND_MAX; 
}
